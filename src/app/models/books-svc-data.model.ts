import { BooksFilter } from './books-filter.model';
import { Book } from './book.model';

export class BooksSvcData {
	filter: BooksFilter = new BooksFilter();
	books: Book[] = [];
}
