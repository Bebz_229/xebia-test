import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { BooksFilterComponent } from './books-filter.component';
import { Utils } from '../../../testing/utils';
import { BooksService } from '../../services/books.service';

describe('BooksFilterComponent', () => {
  let component: BooksFilterComponent;
  let fixture: ComponentFixture<BooksFilterComponent>;
	let booksSvc = Utils.getSvc('BooksService');;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooksFilterComponent ],
			schemas: [ NO_ERRORS_SCHEMA ],
			providers: [
				{ provide: BooksService, useValue: booksSvc }
			]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooksFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
