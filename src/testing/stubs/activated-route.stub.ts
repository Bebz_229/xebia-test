import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { UrlSegment } from '@angular/router';

export class ActivatedRouteStub {
	snapshot: any = {
		data: {}
	};

	url: BehaviorSubject<UrlSegment[]> = new BehaviorSubject<UrlSegment[]>([]);

	setData (key: string, value: any) : void {
		this.snapshot.data[key] = value;
	}
}
