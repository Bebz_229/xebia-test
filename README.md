# XeBook Shop for Xebia
Well, as you can see, this project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.7. Check `README_ANGULAR.md`. And YES the readme will be in english, and almost everything (code, comments, vars, functions...). Why? Because I don't change my coding habits unless I'm told to. :)

## Dev
You'll need to run:
* `npm i` to install the dependencies,
* `npm start` and once started the app will be on http://localhost:4200/

### Folders architecture
`src/app` => main modules and Folders

`src/app/components` => app components all declared in the `comments.module.ts`

`src/app/models` => all models

`src/app/pipes` => `pipes.module.ts` & all pipes

`src/app/resolvers` => all resolvers

`src/app/services` => `services.module.ts` & all services

`src/app/utils` => utils

`testing` => contains all testing utils

## Build
`npm run build`.

If it fails make sure to change the module field in tsconfig.app.json to `"module" : "commonjs"` and try again. This is due to an angular webpack error. Perhaps it has been solved by now. The built app will be in the dist at the root

## Tests
`npm test`
