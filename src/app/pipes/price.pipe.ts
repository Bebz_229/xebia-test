import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'price'
})
export class PricePipe implements PipeTransform {

	currencyPipe: CurrencyPipe = new CurrencyPipe('fr');

  transform(value: any, args?: any): any {
    return this.currencyPipe.transform(value, 'EUR', 'symbol', '.2-2');
  }

}
