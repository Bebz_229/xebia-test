import { TestBed, inject } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { BasketService } from './basket.service';
import { Utils } from '../../testing/utils';
import { BasketItem } from '../models/basket-item.model';
import { BasketSvcData } from '../models/basket-svc-data.model';

let svc: BasketService;
let httpSvc = Utils.getSvc('HttpService');

describe('BasketService', () => {
	beforeEach(() => {
		svc = new BasketService(httpSvc)
	});

	it('should be created', () => {
		expect(svc).toBeTruthy();
		expect(svc.data).toEqual(svc.getBasket());
	});

	describe('#addItem', () => {
		beforeEach(() => {
			spyOn(svc, 'saveBasketItems');
		});

		it('should add item to service items', () => {
			const itemKey = 'a';
			const unitPrice = 10;

			expect(svc.data.items[itemKey]).toBeUndefined();

			svc.addItem(itemKey, unitPrice);
			expect(svc.data.items[itemKey]).toEqual(new BasketItem(unitPrice));
			expect(svc.data.itemsCount).toEqual(1);
			expect(svc.saveBasketItems).toHaveBeenCalled();
		});

		it('should increase item count if item already in basket', () => {
			const itemKey = 'a';
			const unitPrice = 10;

			svc.addItem(itemKey, unitPrice);
			svc.addItem(itemKey, unitPrice);

			expect(svc.data.items[itemKey]).not.toEqual(new BasketItem(unitPrice));
			expect(svc.data.itemsCount).toEqual(2);
			expect(svc.saveBasketItems).toHaveBeenCalled();
			expect(svc.data.items[itemKey].count).toBe(2);
		});
	});

	describe('#removeItem', () => {
		let saveBasketItemsSpy;
		beforeEach(() => {
			saveBasketItemsSpy = spyOn(svc, 'saveBasketItems');
			svc.addItem('a', 10);
			svc.addItem('a', 10);
		});

		it('should do nothing if no item found', () => {
			svc.removeItem('');

			expect(saveBasketItemsSpy.calls.count()).toBe(2);
			expect(svc.data.items['a'].count).toBe(2);
		});

		it('should decrease item count', () => {
			svc.removeItem('a');

			expect(saveBasketItemsSpy.calls.count()).toBe(3); // Partirrrr un jourrrrrrrrrrrrrrrrrr sans retourrrrr 😅
			expect(svc.data.items['a'].count).toBe(1);
		});

		it('should delete item from items', () => {
			svc.removeItem('a');
			svc.removeItem('a');

			expect(saveBasketItemsSpy.calls.count()).toBe(4);
			expect(svc.data.items['a']).toBeUndefined();
		});
	});

	describe('#saveBasketItems', () => {
		it('should save items to sessionStorage', () => {
			const spy = spyOn(window.sessionStorage, 'setItem');

			svc.saveBasketItems();
			expect(spy).toHaveBeenCalledWith('basketItems', JSON.stringify(svc.data.items));
		});
	});

	describe('#getBasket', () => {
		it('should return a basket with no item', () => {
			const expected = svc.getBasket();

			expect(expected).toEqual(jasmine.any(BasketSvcData));
			expect(Object.keys(expected.items).length).toBe(0)
		});

		it('should create items', () => {
			const item = { unitPrice: 10, count: 5 };
			spyOn(window.sessionStorage, 'getItem').and.returnValue(JSON.stringify({ a: item}));
			const expected = svc.getBasket();

			expect(expected).toEqual(jasmine.any(BasketSvcData));
			expect(Object.keys(expected.items).length).toBe(1);
			expect(expected.items['a']).toEqual(jasmine.any(BasketItem));
			expect(expected.items['a']).toEqual(new BasketItem(item.unitPrice, item.count));
			expect(expected.itemsCount).toBe(item.count);
		});
	});

	describe('#updatePrices', () => {
		let spy;

		beforeEach(() => {
			spy = spyOn(svc, 'getDiscounts');
		})

		it('should set prices to 0 as there is no items', () => {
			svc.updatePrices();

			expect(svc.data.totalPrice).toBe(0);
			expect(svc.data.discount).toBe(0);
			expect(svc.data.finalPrice).toBe(0);
			expect(spy).not.toHaveBeenCalled();
		});

		it('should get discounts', () => {
			svc.data.items = {
				a: { unitPrice: 10, count: 5 },
				b: { unitPrice: 5, count: 2 }
			};
			svc.updatePrices();

			expect(svc.data.totalPrice).toBe(60);
			expect(svc.data.discount).toBe(0);
			expect(svc.data.finalPrice).toBe(60);
			expect(spy).toHaveBeenCalledWith('a,a,a,a,a,b,b');
		});
	});

	describe('#getDiscounts', () => {
		it('should get the better offer', () => {
			httpSvc.get.and.returnValue(new Observable(o => {
				o.next({
					offers: [
						{ "type" : "percentage" , "value" : 5 },
						{ "type" : "minus" , "value" : 15 },
						{ "type" : "slice" , "sliceValue" : 100 , "value" : 12 }
					]
				});
				o.complete();
			}));
			svc.data.totalPrice = 65;

			svc.getDiscounts('');

			expect(svc.data.discount).toBe(15);
			expect(svc.data.finalPrice).toBe(50);
		});
	});

	describe('#proceedPayment', () => {
		it('should empty basket', () => {
			let spy = spyOn(window.sessionStorage, 'removeItem');
			svc.data.items = { a: 1 };
			svc.data.itemsCount = 42;
			svc.proceedPayment();

			expect(svc.data.items).toEqual({});
			expect(svc.data.itemsCount).toBe(0);
			expect(spy).toHaveBeenCalledWith('basketItems');
		});
	});
});
