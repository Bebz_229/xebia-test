import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BookDetailsComponent } from './book-details.component';
import { ActivatedRouteStub } from '../../../testing/stubs/activated-route.stub';

describe('BookDetailsComponent', () => {
  let component: BookDetailsComponent;
  let fixture: ComponentFixture<BookDetailsComponent>;
	let route: ActivatedRouteStub = new ActivatedRouteStub();

	const book = { id: 5 };

	route.setData('book', book);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookDetailsComponent ],
			schemas: [ NO_ERRORS_SCHEMA ],
			providers: [
				{ provide: ActivatedRoute, useValue: route }
			]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
