import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { CartComponent } from './cart.component';
import { Utils } from '../../../testing/utils';
import { BasketService } from '../../services/basket.service';
import { BooksService } from '../../services/books.service';
import { PricePipe } from '../../pipes/price.pipe';

describe('CartComponent', () => {
  let component: CartComponent;
  let fixture: ComponentFixture<CartComponent>;
	let basketSvc = Utils.getSvc('BasketService');
	let booksSvc = Utils.getSvc('BooksService');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartComponent, PricePipe ],
			schemas: [ NO_ERRORS_SCHEMA ],
			providers: [
				{ provide: BasketService, useValue: basketSvc },
				{ provide: BooksService, useValue: booksSvc }
			]
    })
    .compileComponents();
  }));

  beforeEach(() => {

		booksSvc.getBooks.and.returnValue(new Observable(o => {
			o.next([]);
			o.complete();
		}));

    fixture = TestBed.createComponent(CartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
