import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { BasketSvcData } from '../../models/basket-svc-data.model';
import { BasketService } from '../../services/basket.service';

@Component({
	selector: 'app-cart-actions',
	templateUrl: './cart-actions.component.html',
	styleUrls: ['./cart-actions.component.css']
})
export class CartActionsComponent implements OnInit {

	@Input() itemKey: string;
	@Input() unitPrice: number;

	basketData: BasketSvcData;
	max: number = 5;
	mustUpdatePrices: boolean

	constructor(private basketSvc: BasketService, private route: ActivatedRoute) { }

	ngOnInit() {
		this.basketData = this.basketSvc.data;
		this.route.url.subscribe(e => {
			this.mustUpdatePrices = !!(e[0] && e[0].path === 'cart');
		})
	}

	getCount () : number {
		return this.basketData.items[this.itemKey] ? this.basketData.items[this.itemKey].count : 0;
	}

	addToCart () : void {
		if (this.getCount() < this.max) {
			this.basketSvc.addItem(this.itemKey, this.unitPrice);
			this.updateBasketPrices();
		}
	}

	removeFromCart () : void {
		if (this.getCount() > 0) {
			this.basketSvc.removeItem(this.itemKey);
			this.updateBasketPrices()
		}
	}

	updateBasketPrices () : void {
		if (this.mustUpdatePrices) {
			this.basketSvc.updatePrices();
		}
	}

}
