import { Component, OnInit, Input } from '@angular/core';

import { Book } from '../../models/book.model';

@Component({
	selector: 'app-books',
	templateUrl: './books.component.html',
	styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

	@Input() books: Book[];
	@Input() booksGrid: string;
	@Input() hideBooksCount: boolean;
	@Input() hideBooksActionButtons: boolean;

	constructor() { }

	ngOnInit() {
	}

}
