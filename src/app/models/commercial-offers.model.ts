export class CommercialOffers {
	offers: Offer[]
}

class Offer {
	type: string;
	value: number;
	sliceValue: number;
}
