export const debounce = (func: Function, wait: number = 250, immediate: boolean, ctx: any = null) => {
    let timeout;
    return function() {
        let context = ctx || this, args = arguments;
        let later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
