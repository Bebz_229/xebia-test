import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { BooksService } from './books.service';
import { HttpService } from './http.service';
import { BooksSvcData } from '../models/books-svc-data.model';
import { Utils } from '../../testing/utils';

let svc;
let httpSvc = Utils.getSvc('HttpService');
let books = [{ isbn: 'a' }, { isbn: 'z' }, { isbn: 'e' }];

describe('BooksService', () => {
	beforeEach(() => {
		svc = new BooksService(httpSvc as HttpService);
	});

	it('should be created', () => {
		expect(svc).toBeTruthy();
		expect(svc.data).toEqual(jasmine.any(BooksSvcData));
		expect(svc.books$).toEqual(jasmine.any(BehaviorSubject));
		expect(svc.books).toEqual([]);
	});

	describe('#getBooks', () => {

		beforeEach(() => {
			httpSvc.get.and.returnValue(Observable.of(books))
		});

		it('should update books', () => {
			spyOn(svc, 'filterBooks');

			svc.getBooks().subscribe(b => {
				expect(httpSvc.get).toHaveBeenCalled();
				expect(svc.books).toEqual(books);
				expect(svc.filterBooks).toHaveBeenCalled();
				expect(b).toEqual(books);
			});
		});
	});

	describe('#filterBooks', () => {
		beforeEach(() => {
			svc.books$.next(books);
		});

		it('should not filter', () => {
			svc.filterBooks();

			expect(svc.books).toEqual(books);
			expect(svc.data.books).toEqual(books);
		});

		it('should filter', () => {
			svc.data.filter.any = 'toto';
			svc.filterBooks();

			expect(svc.books).toEqual(books);
			expect(svc.data.books.length).toBe(0);
		});
	});

	describe('#includes', () => {
		it('should includes despite of camel case', () => {
			expect(svc.includes('toToazerty', 'TOTO')).toBeTruthy();
		});

		it('should includes if no values are provided', () => {
			expect(svc.includes()).toBeTruthy();
		});

		it('should not includes if container does not', () => {
			expect(svc.includes('', 'toto')).toBeFalsy();
			expect(svc.includes('tot', 'toto')).toBeFalsy();
		})
	});

	describe('#hasBooks', () => {
		it('should not have books', () => {
			expect(svc.hasBooks()).toBeFalsy();
		});

		it('should have books', () => {
			svc.books$.next(books);

			expect(svc.hasBooks()).toBeTruthy();
		});
	});

	describe('#getBasketBooks', () => {
		it('should not return empty list if no params provided', () => {
			expect(svc.getBasketBooks()).toEqual([]);
		});

		it('should return filtered list', () => {
			svc.books$.next(books);

			let filtered = svc.getBasketBooks(['a', 'z', 'zozo']);

			expect(filtered).toEqual([{ isbn: 'a' }, { isbn: 'z' }])
		});
	});

	describe('#getBook', () => {
		it('should retrieve book list before getting book', () => {
			spyOn(svc, 'getBooks').and.callFake(() => {
				svc.books$.next(books);
				return Observable.of(books);
			});

			svc.getBook('a').subscribe(b => {
				expect(svc.getBooks).toHaveBeenCalled();
				expect(b).toEqual(books[0])
			});
		});

		it('should not retrieve books', () => {
			spyOn(svc, 'getBooks');
			svc.books$.next(books);

			svc.getBook('a').subscribe(b => {
				expect(svc.getBooks).not.toHaveBeenCalled();
				expect(b).toEqual(books[0])
			});
		});
	});
});
