import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { FormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { RoutingModule } from '../routing.module';

import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { BooksFilterComponent } from './books-filter/books-filter.component';
import { BooksComponent } from './books/books.component';
import { BookComponent } from './book/book.component';
import { CartActionsComponent } from './cart-actions/cart-actions.component';
import { CartComponent } from './cart/cart.component';
import { PipesModule } from '../pipes/pipes.module';
import { BookDetailsComponent } from './book-details/book-details.component';
import { ActionButtonComponent } from './action-button/action-button.component';

registerLocaleData(localeFr, 'fr');

@NgModule({
	imports: [
		CommonModule,
		MaterialModule,
		FormsModule,
		RoutingModule,
		PipesModule
	],
	exports: [
		RoutingModule,
		HeaderComponent
	],
	declarations: [HeaderComponent, HomeComponent, BooksFilterComponent, BooksComponent, BookComponent, CartActionsComponent, CartComponent, BookDetailsComponent, ActionButtonComponent]
})
export class ComponentsModule { }
