export class BasketItem {
	count: number = 1;
	unitPrice: number = 0;

	constructor (unitPrice: number, count: number = 1) {
		this.unitPrice = unitPrice;
		this.count = count;
	}

	increase () : void {
		this.count++;
	}

	decrease () : void {
		this.count--;
	}
}
