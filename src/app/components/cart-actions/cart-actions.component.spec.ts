import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CartActionsComponent } from './cart-actions.component';
import { Utils } from '../../../testing/utils';
import { ActivatedRouteStub } from '../../../testing/stubs/activated-route.stub';
import { BasketService } from '../../services/basket.service';

describe('CartActionsComponent', () => {
  let component: CartActionsComponent;
  let fixture: ComponentFixture<CartActionsComponent>;
	let basketSvc = Utils.getSvc('BasketService');
	let route = new ActivatedRouteStub();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartActionsComponent ],
			schemas: [ NO_ERRORS_SCHEMA ],
			providers: [
				{ provide: BasketService, useValue: basketSvc },
				{ provide: ActivatedRoute, useValue:  route }
			]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
