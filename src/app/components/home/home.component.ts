import { Component, OnInit } from '@angular/core';

import { BooksSvcData } from '../../models/books-svc-data.model';
import { BooksService } from '../../services/books.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	booksData: BooksSvcData;

	constructor(private booksSvc: BooksService) { }

	ngOnInit() {
		this.booksData = this.booksSvc.data;

		if (!this.booksSvc.hasBooks()) {
			this.booksSvc.getBooks();
		}
	}

}
