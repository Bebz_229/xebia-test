import { Injectable } from '@angular/core';

import { BasketSvcData } from '../models/basket-svc-data.model';
import { BasketItem } from '../models/basket-item.model';
import { CommercialOffers } from '../models/commercial-offers.model';
import { HttpService } from '../services/http.service';

@Injectable()
export class BasketService {

	data: BasketSvcData = this.getBasket();

	constructor(private httpSvc: HttpService) { }

	/**
	* Create item in basket if it doesn't exist, and increase count
	* @param itemKey   key in basket items
	* @param unitPrice item unit price
	*/
	addItem (itemKey: string, unitPrice: number) : void {
		const item = this.data.items[itemKey];

		if (item) {
			item.increase();
		} else {
			this.data.items[itemKey] = new BasketItem(unitPrice);
		}

		this.data.itemsCount++;
		this.saveBasketItems();
	}

	/**
	* decrease item count in basket
	* @param itemKey key in basket items
	*/
	removeItem (itemKey: string) : void {
		const item = this.data.items[itemKey];

		if (item) {
			item.decrease();
			this.data.itemsCount--;

			if (!item.count) {
				delete this.data.items[itemKey];
			}

			this.saveBasketItems();
		}
	}

	saveBasketItems () : void {
		window.sessionStorage.setItem('basketItems', JSON.stringify(this.data.items));
	}

	/**
	 * check if a basket items are stored in localStorage in order to init basket svc with right data value
	 * @return a new BasketSvcData
	 */
	getBasket () : BasketSvcData {
		const savedBasketItems = JSON.parse(window.sessionStorage.getItem('basketItems'));
		const basket = new BasketSvcData();

		if (savedBasketItems) {
			Object.keys(savedBasketItems).forEach(k => {
				const item = savedBasketItems[k];

				basket.items[k] = new BasketItem(item.unitPrice, item.count);
				basket.itemsCount += item.count;
			});
		}

		return basket;
	}

	/**
	 * Updates prices in basket but summing items and getting commercial offers
	 */
	updatePrices () : void {
		let total = 0;
		let isbnStr = '';

		const { items } = this.data;

		Object.keys(items).forEach(k => {
			const { count, unitPrice } = items[k];

			total += count * unitPrice;
			isbnStr += (k + ',').repeat(count);
		});

		isbnStr = isbnStr.slice(0, -1);

		this.data.totalPrice = total;
		this.data.discount = 0;

		if (isbnStr) {
			this.getDiscounts(isbnStr);
		}
	}

	/**
	 * gets commercial offers
	 * @param isbnStr items isbn api url formatted
	 */
	getDiscounts (isbnStr: string) : void {
		const { totalPrice } = this.data;
		const url = `http://henri-potier.xebia.fr/books/${ isbnStr }/commercialOffers`;

		this.httpSvc.get<CommercialOffers>(url).subscribe(commercialOffers => {
			let maxDiscount = 0;

			commercialOffers.offers.forEach(offer => {
				let discount = 0;
				const { type, value, sliceValue } = offer;

				if (type === 'percentage') {
					discount = totalPrice * value / 100;
				} else if (type === 'minus') {
					discount = value;
				} else if (sliceValue) {
					discount = Math.floor(totalPrice / sliceValue) * value;
				}

				if (discount > maxDiscount) {
					maxDiscount = discount;
				}
			});

			this.data.discount = maxDiscount;
		});
	}

	proceedPayment () : void {
		this.data.items = {};
		this.data.itemsCount = 0;
		window.sessionStorage.removeItem('basketItems');
	}
}
