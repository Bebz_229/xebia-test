import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { BooksService } from './books.service';
import { HttpService } from './http.service';
import { BasketService } from './basket.service';

@NgModule({
	imports: [
		HttpClientModule
	],
	declarations: [],
	providers: [BooksService, HttpService, BasketService]
})
export class ServicesModule { }
