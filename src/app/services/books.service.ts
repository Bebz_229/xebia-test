import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { BooksSvcData } from '../models/books-svc-data.model';
import { HttpService } from './http.service';
import { Book } from '../models/book.model';

@Injectable()
export class BooksService {

	data: BooksSvcData = new BooksSvcData();
	books$: BehaviorSubject<Book[]> = new BehaviorSubject<Book[]>([]);

	constructor(private httpSvc: HttpService) {}

	get books () {
		return this.books$.getValue();
	}

	/**
	 * fetch books and update service books
	 * @return a books subject so that any component can subscribe if really needed
	 */
	getBooks () : BehaviorSubject<Book[]> {
		const url = 'http://henri-potier.xebia.fr/books';

		this.httpSvc.get<Book[]>(url).subscribe(b => {
			this.books$.next(b);
			this.filterBooks();
		});

		return this.books$
	}

	/**
	 * filter books on all text alike fields
	 */
	filterBooks () : void {
		let filteredBooks = [...this.books];

		const { any: filterValue } = this.data.filter;

		if (filterValue) {
			filteredBooks = filteredBooks.filter(b => (this.includes(b.title, filterValue) || b.synopsis && b.synopsis.length && !!b.synopsis.find(s => this.includes(s, filterValue)) || this.includes(b.isbn, filterValue) || this.includes(b.cover, filterValue)))
		}

		this.data.books = filteredBooks;
	}

	includes (value: string = '', filterValue: string = '') {
		return value.toLowerCase().includes(filterValue.toLowerCase())
	}

	hasBooks () : boolean {
		return !!(this.books && this.books.length);
	}

	getBasketBooks (isbnList: string[] = []) : Book[] {
		if (!isbnList.length) { return []; }

		return this.books.filter(b => isbnList.includes(b.isbn));
	}

	/**
	 * async get book by isbn
	 * @param  isbn
	 * @return book as an observable
	 */
	getBook (isbn: string) : Observable<Book> {
		return new Observable<Book>(observer => {
				const updateObserver = () => {
					const book = this.books.find(b => b.isbn === isbn);
					observer.next(book);
					observer.complete();
				}

				if (this.hasBooks()) {
					updateObserver();
				} else {
					this.getBooks().subscribe(bs => {
						if (bs.length) {
							updateObserver();
						}
					});
				}
		});
	}
}
