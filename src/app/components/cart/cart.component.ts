import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { Book } from '../../models/book.model';
import { BooksService } from '../../services/books.service';
import { BasketService } from '../../services/basket.service';
import { BasketSvcData } from '../../models/basket-svc-data.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {

	books: Book[];
	basketData: BasketSvcData;
	subscription: Subscription;


  constructor(private booksSvc: BooksService, private basketSvc: BasketService) { }

  ngOnInit() {
		this.basketData = this.basketSvc.data;

		if (this.booksSvc.hasBooks()) {
			this.setBooks();
		} else {
			this.subscription = this.booksSvc.getBooks().subscribe(b => {
				if (b.length) {
					this.setBooks();
				}
			});
		}
  }

	setBooks () : void {
		const isbnList = Object.keys(this.basketSvc.data.items);

		this.books = this.booksSvc.getBasketBooks(isbnList);
		this.basketSvc.updatePrices();
	}

	ngOnDestroy () {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}

	getBooksGrid () : string {
		return 'col-xs-12 col-sm-' + (this.books && this.books.length <= 1 ? '12' : '6');
	}

	proceedPayment () {
		this.basketSvc.proceedPayment();
		this.books = null;
	}

}
