import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

	subscription: Subscription;

	constructor(private router: Router) {}

	ngOnInit () {
		this.subscription = this.router.events.subscribe(e => {
			if (e instanceof NavigationEnd) {
				window.scroll(0, 0);
			}
		});
	}

	ngOnDestroy () {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}
	}
}
