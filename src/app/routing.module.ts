import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { CartComponent } from './components/cart/cart.component';
import { BookDetailsComponent } from './components/book-details/book-details.component';
import { BookResolver } from './resolvers/book.resolver';

const routes: Routes = [
    { path: '', component: HomeComponent },
		{ path: 'cart', component: CartComponent },
		{ path: 'book/:isbn', component: BookDetailsComponent, resolve: { book: BookResolver } },
    { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [
      RouterModule.forRoot(routes)
  ],
  exports: [
      RouterModule
  ],
  declarations: [],
	providers: [BookResolver]
})
export class RoutingModule { }
