import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { AppComponent } from './app.component';
import { RouterStub } from '../testing/stubs/router.stub';

let fixture: ComponentFixture<AppComponent>;
let app: AppComponent;

describe('AppComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				AppComponent
			],
			imports: [ RouterTestingModule ],
			schemas: [ NO_ERRORS_SCHEMA ],
			providers: [
				{ provide: Router, useValue: new RouterStub() }
			]
		}).compileComponents();

		fixture = TestBed.createComponent(AppComponent);
		app = fixture.debugElement.componentInstance;
	}));

	it('should create the app', async(() => {
		expect(app).toBeTruthy();
	}));

	it('should have subscription', async(() => {
		expect(app.subscription).toBeUndefined();

		fixture.detectChanges();

		expect(app.subscription).toBeDefined();
	}));

	it('should unsubscribe from subscription on destroy', async(() => {
		fixture.detectChanges();

		spyOn(app.subscription, 'unsubscribe')
		app.ngOnDestroy();

		expect(app.subscription.unsubscribe).toHaveBeenCalled();
	}));

	it('should scroll when route changes', async(() => {
		const router = TestBed.get(Router);

		spyOn(window, 'scroll');
		fixture.detectChanges();
		router.navigate('rr');

		expect(window.scroll).toHaveBeenCalledWith(0, 0);
	}));
});
