import { Component, OnInit } from '@angular/core';

import { BasketService } from '../../services/basket.service';
import { BasketSvcData } from '../../models/basket-svc-data.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

	basketData: BasketSvcData;

  constructor(private basketSvc: BasketService) { }

  ngOnInit() {
		this.basketData = this.basketSvc.data;
  }

}
