import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PricePipe } from './price.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
	exports: [PricePipe],
  declarations: [PricePipe]
})
export class PipesModule { }
