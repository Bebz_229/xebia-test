import { Component, OnInit } from '@angular/core';
import { MatExpansionPanel } from '@angular/material/expansion';

import { BooksService } from '../../services/books.service';
import { BooksSvcData } from '../../models/books-svc-data.model';
import { debounce } from '../../utils/debounce.utils';

@Component({
    selector: 'app-books-filter',
    templateUrl: './books-filter.component.html',
    styleUrls: ['./books-filter.component.css']
})
export class BooksFilterComponent implements OnInit {

    booksSvcData: BooksSvcData;
    filterBooks: Function;

    constructor(private booksSvc: BooksService) { }

    ngOnInit() {
        this.booksSvcData = this.booksSvc.data;
        this.filterBooks = debounce(this.booksSvc.filterBooks, 250, false, this.booksSvc);
    }

    checkPanel (event: any, panel: MatExpansionPanel) {
        const previousExpanded = !panel.expanded

        if ((event.target.type && !previousExpanded) || (!event.target.type && previousExpanded) ) {
            panel.close();
        } else {
            panel.open();
        }
    }

		ignorePanelOpen (event: any, searchInput: HTMLInputElement) {
			const excludedKeyCodes = [32, 13];

			if (excludedKeyCodes.includes(event.keyCode)) {
				event.stopPropagation();
			}

			// To close keyboard on mobile devices as event propagation has been stopped
			if (event.keyCode === 13) {
				searchInput.blur()
			}
		}
}
