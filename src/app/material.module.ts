import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
	imports: [
		BrowserAnimationsModule
	],
	exports: [
		MatToolbarModule,
		MatExpansionModule,
		MatInputModule,
		MatButtonModule,
		MatIconModule
	],
	declarations: []
})
export class MaterialModule { }
