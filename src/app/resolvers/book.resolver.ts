import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Book } from '../models/book.model';
import { BooksService } from '../services/books.service';

@Injectable()
export class BookResolver implements Resolve<Book> {
	constructor (private booksSvc: BooksService) {}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<Book> {
		return this.booksSvc.getBook(route.paramMap.get('isbn'));
	}
}
