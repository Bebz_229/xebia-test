import { NavigationEnd } from '@angular/router';
import { Subject } from "rxjs";

export class RouterStub {
	public url: string;
	private subject = new Subject();
	public events = this.subject.asObservable();

	navigate(url: string) : void {
		this.url = url;
		this.triggerNavEvents(url);
	}

	triggerNavEvents(url) : void {
		let ne = new NavigationEnd(0, url, null);
		this.subject.next(ne);
	}
}
