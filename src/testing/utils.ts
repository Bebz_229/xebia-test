import { BooksSvcData } from '../app/models/books-svc-data.model';
import { BasketSvcData } from '../app/models/basket-svc-data.model';

export class Utils {
	private static services = {
		HttpService: {
			methods: ['get']
		},
		BooksService: {
			data: new BooksSvcData(),
			methods: ['filterBooks', 'hasBooks', 'getBooks']
		},
		BasketService: {
			data: new BasketSvcData()
		}
	};

	static getSvc (name) {
		const exists = this.services[name];
		let svc =  exists && exists.methods ? jasmine.createSpyObj(name, exists.methods) : {};

		svc = {...exists, ...svc }
		delete svc.methods;

		return svc;
	}
}
