export class BasketSvcData {
	items: object = {};
	discount: number = 0;
	totalPrice: number = 0;
	itemsCount: number = 0;

	get finalPrice() : number {
		return this.totalPrice - this.discount;
	}
}
